<?php
class Z1 {
    
    public function __construct($parameters = array()) {
        foreach($parameters as $key => $value) {
            $this->$key = $value;
        }
    }

    public function sumEven(){
        $even =0;
        foreach($this as $key => $value){
            if($this->$key % 2 == 0){
                $even+=$this->$key;
            }
        }
        return $even;
    }
    
    public function sumOdd(){
        $odd=0;
        foreach($this as $key => $value){
            if($this->$key % 2 !== 0):
                $odd+=$this->$key;
            endif;
        }
        return $odd;
    }
 
    public function getResult(){
        $even = $this->sumEven();
        $even=3*$even;
        $odd = $this->sumOdd();
        $odd=2*$odd;
        if($even > $odd):
            echo 'Even multiplied by 3 was bigger than odd multiplied by 2!';
        else:
            echo 'Odd multiplied by 2 was bigger than even multiplied by 3!';
        endif;
    }
}

$testArray = array(1,5,7,2);
if (class_exists('Z1')):
    $test = new Z1($testArray);
    if(method_exists($test,'getResult')):
        echo $test->getResult();
    endif;
endif;
?>