<?php
/**
 * @Entity @Table(name="products")
 **/

class Product
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $nazwa; 
    /** @Column(type="text") **/
    protected $opis;
    /** @Column(type="integer") **/
    protected $cena;
    /** @Column(type="boolean") **/
    protected $dostepnosc;
    /**
     * @ManyToMany(targetEntity="Category")
     **/
    private $category;
    
    public function getId()
    {
        return $this->id;
    }

    public function getNazwa()
    {
        return $this->nazwa;
    }

    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    }
    
    public function getOpis()
    {
        return $this->opis;
    }
    
    public function setOpis($opis)
    {
        $this->opis = $opis;
    }
    
    public function getCena()
    {
        return $this->cena;
    }
    
    public function setCena($cena)
    {
        $this->cena = $cena;
    }
    
    public function getDostepnosc()
    {
        return $this->dostepnosc;
    }
    
    public function setDostepnosc($dostepnosc)
    {
        $this->dostepnosc =$dostepnosc;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add category.
     *
     * @param \Category $category
     *
     * @return Product
     */
    public function addCategory(\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category.
     *
     * @param \Category $category
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCategory(\Category $category)
    {
        return $this->category->removeElement($category);
    }

    /**
     * Get category.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }
}
