<?php
// src/ProductRepository.php

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function getAvailableProductsCount()
    {
        $sql = "SELECT COUNT(id) FROM products WHERE dostepnosc=true";
        
        $query = $this->getEntityManager()->createQuery($sql);
        return $query->getResult();
    }
    
    public function getNonAvailableProducts()
    {
        $sql = "SELECT * FROM products WHERE dostepnosc=false";
        return $this->getEntityManager()->createQuery($sql)->getResult();
    }
    
    public function getProductsByName($name)
    {
        $sql = "SELECT * FROM products WHERE nazwa=?1";
        return $this->getEntityManager()->createQuery($sql)
                             ->setParameter(1, $name)
                             ->getResult();
    }
}
?>