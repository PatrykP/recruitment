<?php
// src/CategoryRepository.php

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function getProductsFromCategory($categoryId)
    {
        $sql = "SELECT p, c FROM products p JOIN category c WHERE c.id=?1 AND p.dostepnosc=true";
        $query = $this->getEntityManager()->createQuery($sql)->setParameter(1, $categoryId);
        return $query->getResult();
    }
    
    public function getProductsFromCategorySorted($categoryId){
        $sql = "SELECT p, c FROM products p JOIN category c WHERE c.id=?1 AND p.dostepnosc=true ORDER BY p.nazwa";
        $query = $this->getEntityManager()->createQuery($sql)->setParameter(1, $categoryId);
        return $query->getResult();
    }
    
}
?>