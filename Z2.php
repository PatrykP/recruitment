<?php include 'Z5.php'; ?>
<!DOCTYPE html>
<html lang="pl">
    
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="css/style.css" />

</head>

<body>
<header>
    <div id="menu"><ul class="navMenu"><?php echo get_menu(0); ?></ul></div>
</header>
<main>

<section id="form">
    <form action="" method="" name="userForm" >
        <p>Imię</p>
        <input type="text" name="firstName"/><p id="firstNameOutput" class="error"></p>
        <p>Nazwisko</p>
        <input type="text" name="surrName" /><p id="surrNameOutput" class="error"></p>
        <p>Wiek</p>
        <input type="number" name="age" /><p id="ageOutput" class="error"></p>
        <p>Płeć</p>
        <select>
            <option value="mezczyzna">Mezczyzna</option>
            <option value="kobieta">Kobieta</option>
        </select><p class="error" id="sexOutput"></p>
        <p><input type="checkbox" name="checkBox"/><span>Zgoda na przetwarzanie danych osobowych</span></p>
    </form>
    <button onClick="validate();">Prześlij formularz</button>
</section>   
</main>
    <aside class="half"><div id="cv"></div></aside>
<!-- SCRIPT SECTION -->
<script src="pdfobject.js"></script>
<script>

    
//PDF section
var options = {
	pdfOpenParams: {
		pagemode: "thumbs",
		navpanes: 0,
		toolbar: 0,
		statusbar: 0,
		view: "FitV"
	}
};

var myPDF = PDFObject.embed("PatrykPusleckiCV.pdf", "#cv", options);

//end of PDF section
    
//Form validation section
var firstNameOutput = document.getElementById("firstNameOutput");
var surrNameOutput = document.getElementById("surrNameOutput");
var ageOutput = document.getElementById("ageOutput");
var sexOutput = document.getElementById("sexOutput");
    
var firstName = document.userForm.firstName;
var surrName = document.userForm.surrName;
var age = document.userForm.age;
var sex = document.userForm.sex;
var checkBox = document.userForm.checkBox;

    function validate(){
        if (surrName.value == ""){
            surrNameOutput.innerHTML = "To pole jest wymagane!";
        }
        
        if (firstName.value == ""){
            firstNameOutput.innerHTML = "To pole jest wymagane!";
        }
        
        if(checkBox.checked == false){
            window.alert("Zgoda na przetwarzanie danych osobowych jest wymagana!");
        }
        console.log(age.value);
        if(age.value == "" ){
            ageOutput.innerHTML = "To pole jest wymagane!";
        } else if(age.value < 18 || age.value > 99){
            ageOutput.innerHTML = "Wiek musi być w przedziale 18-99!";
        }
    }
//end of form validation section
</script>

</body>
</html>