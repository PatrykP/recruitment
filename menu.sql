CREATE TABLE IF NOT EXISTS menu (
    id INT AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    link VARCHAR(255) NOT NULL,
    parent_id INT NOT NULL DEFAULT '0',
    PRIMARY KEY(id)
);

INSERT INTO menu (title,link,parent_id) VALUES
('Podkategoria 1','/','0'),
('Podkategoria 1.1','/','1'),
('Podkategoria 1.2','/','1'),
('Podkategoria 2','/','0'),
('Podkategoria 2.1','/','4'),
('Podkategoria 2.2','/','4');